import java.util.ArrayList;

/**
 * Stores all observers, this class represents the subject being 'observed'.
 */
public class SensorSystem
{
    ArrayList<AlarmListener> listeners;

    /**
     * Create a publisher instance which notifies all observers.
     */
    public SensorSystem()
    {
        listeners = new ArrayList<AlarmListener>();
    }

    /**
     * Add an observer.
     */
    public void register(AlarmListener alarmListener)
    {
        listeners.add(alarmListener);
    }

    /**
     * Notify each observer.
     */
    public void soundTheAlarm()
    {
        for (AlarmListener listener : listeners)
            listener.alarm();
    }
}
    
