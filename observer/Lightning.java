/**
 * Concrete observer implementation.
 */
public class Lightning implements AlarmListener
{
    public Lightning()
    {
    }

    public void alarm()
    {
        System.out.println("lights up");
    }
}
