/**
 * Concrete observer implementation.
 */
public class Surveillance implements AlarmListener
{
    public Surveillance()
    {
    }

    public void alarm()
    {
        System.out.println("Surveillance - by the numbers: ");
    }
}
