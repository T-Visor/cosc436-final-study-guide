#!/bin/sh

# Compile source files and run the java program.
javac *.java && java ObserverDemo

# remove all class files to reduce clutter
rm *.class
