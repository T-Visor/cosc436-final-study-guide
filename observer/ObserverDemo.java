/**
 * Demonstrate the observer design pattern.
 */
public class ObserverDemo 
{
    public static void main( String[] args ) 
    {
        SensorSystem sensorSystem = new SensorSystem();

        // Add all observers.
        sensorSystem.register(new Gates());
        sensorSystem.register(new Lightning());
        sensorSystem.register(new Surveillance());

        // Play their sounds.
        sensorSystem.soundTheAlarm();
    }
}

