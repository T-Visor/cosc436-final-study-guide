/**
 * Concrete observer implementation.
 */
public class Gates implements AlarmListener
{
    public Gates()
    {
    }

    public void alarm()
    {
        System.out.println("Gates closed");
    }
}
