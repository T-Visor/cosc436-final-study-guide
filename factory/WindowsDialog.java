/**
 * A concrete 'creator' for rendering a button for the 
 * Windows operating system.
 */
public class WindowsDialog extends Dialog
{
   public Button createButton()
   {
        return new WindowsButton();
   }
}
