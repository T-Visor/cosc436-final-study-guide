/**
 * A concrete 'creator' for HTML buttons.
 */
public class HtmlDialog extends Dialog
{
   public Button createButton()
   {
        return new HtmlButton();
   }
}
