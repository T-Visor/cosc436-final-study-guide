/**
 * Represents the base 'product' in this factory design pattern.
 */
public interface Button
{
    public void render();

    public void onClick();
}
