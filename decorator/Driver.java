/**
 * Demonstrate the decorator design pattern,
 * innermost element is the base component and outermost is the latest
 * addition.
 */
public class Driver
{
    public static void main(String[] args)
    {
        // Create the widget to be decorated.
        Widget widget;

        // Basic text field.
        widget = new TextField(80, 24);
        widget.draw();
        System.out.println();

        // Text field with scroll bar. 
        widget = new ScrollDecorator(widget); 
        widget.draw();
        System.out.println();

        // Text field with scroll bar and border decorator.
        widget = new BorderDecorator(widget); 
        widget.draw();        
        System.out.println();
    }
}
