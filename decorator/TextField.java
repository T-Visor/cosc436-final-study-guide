/**
 * This is the core component which can be extended with decorators.
 */
public class TextField implements Widget
{
    private int height;
    private int width;

    /**
     * Create a text field instance.
     *
     * @param height height of the text field
     * @param widgth width of the text field
     */
    public TextField(int height, int width)
    {
        this.height = height;
        this.width = width;
    }

    /**
     * Display the text field dimensions.
     */
    public void draw()
    {
        System.out.println("This is a text field" + 
                           "\nheight: " + height + 
                           "\nwidth: " + width);
    }
}
