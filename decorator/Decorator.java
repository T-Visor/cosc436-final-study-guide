/**
 * Top-level decorator class
 */
public class Decorator implements Widget
{
    Widget widget; // aggregation relationship

    public Decorator(Widget widget)
    {
        this.widget = widget;
    }

    /**
     * Call the attached object's draw
     */
    public void draw()
    {
        widget.draw();
    }
}
