/**
 * This is the common interface shared by all classes.
 * (decorators and base component)
 */
public interface Widget
{
    /**
     * The common method which all components must share
     */
    public void draw();
}
