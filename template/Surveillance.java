/**
 * This class is an observer AND inherits the template methods.
 */
public class Surveillance extends CheckList implements AlarmListener
{
    public Surveillance()
    {
    }

    public void alarm()
    {
        System.out.println("Surveillance - by the numbers: ");
        byTheNumbers();
    }

    /**
     * Redefine the isolate() method.
     */
    protected void isolate()
    {
        System.out.println("\ttrain the cameras **THIS LINE DIFFERS FROM PARENT CLASS**");
    }
}
