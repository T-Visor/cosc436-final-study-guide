/**
 * Template class to implement - this class contains methods which are common
 * to a set of other classes.
 * 
 * Subclasses simply redefine any methods which need to be changed.
 */
public abstract class CheckList
{
    /**
     * The method which contains the common order of method calls.
     */
    public void byTheNumbers() 
    {
        localize();
        isolate();
        identify();
    }

    /**
     * Common method (can be redefined).
     */
    protected void localize() 
    {
        System.out.println("\testablish a perimeter");
    }

    /**
     * Common method (can be redefined).
     */
    protected void isolate() 
    {
        System.out.println("\tisolate the grid");
    }

    /**
     * Common method (can be redefined).
     */
    protected void identify() 
    {
        System.out.println("\tidentify the source");
    }
}
