/**
 * Interface for observer objects.
 */
public interface AlarmListener
{
    /**
     * Specifies some action associated with the observer class.
     */
    public void alarm();
}
