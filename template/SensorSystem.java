import java.util.ArrayList;

/**
 * Stores all observers, a publisher class.
 */
public class SensorSystem
{
    ArrayList<AlarmListener> listeners;

    public SensorSystem()
    {
        listeners = new ArrayList<AlarmListener>();
    }

    /**
     * Add an observer.
     */
    public void register(AlarmListener alarmListener)
    {
        listeners.add(alarmListener);
    }

    /**
     * Notify all observers.
     */
    public void soundTheAlarm()
    {
        for (AlarmListener listener : listeners)
            listener.alarm();
    }
}
    
