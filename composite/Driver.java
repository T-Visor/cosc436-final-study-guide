public class Driver
{
    public static void main(String[] args)
    {
        // make some directories
        Directory root = new Directory("/");
        Directory users = new Directory("Users");
        Directory username = new Directory("akira");
        Directory guest = new Directory("guest");

        // nest the directories
        root.add(users);
        users.add(username);
        users.add(guest);

        // add some files
        File file1 = new File("malware.sh");
        File file2 = new File("myVulnerableFile.txt");
        username.add(file1);
        username.add(file2);

        // print all contents from root
        root.ls();
    }
}
