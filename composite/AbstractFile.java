/**
 * Represents a Unix file: can be a file, directory, etc.
 */
public interface AbstractFile
{
    /**
     * Unix-style ls program: list all directory contents
     */
    public void ls();
}
