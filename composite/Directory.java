import java.util.ArrayList;

/**
 * Represents a Unix directory, which contains a set of files.
 */
public class Directory implements AbstractFile
{
    private String name;
    private ArrayList<AbstractFile> includedFiles;

    /**
     * Create a directory instance.
     *
     * @param name the name of the directory
     */
    public Directory(String name)
    {
        this.name = name; 
        includedFiles = new ArrayList<AbstractFile>();
    }

    /**
     * Add a file or directory.
     */
    public void add (AbstractFile file)
    {
        includedFiles.add(file);
    }

    /**
     * Print the contents of the directory.
     */
    public void ls()
    {
        System.out.println("-----Directory: " + name);

        for (AbstractFile file : includedFiles)
            file.ls(); 
    }
}
