/**
 * Represents a single file instance on a Unix system.
 */
public class File implements AbstractFile
{
    private String name;

    /**
     * Create a file instance.
     *
     * @param name the name of the file
     */
    public File(String name)
    {
        this.name = name; 
    }

    /**
     * Print the file name.
     */
    public void ls()
    {
        System.out.println("File: " + name);
    }
}
