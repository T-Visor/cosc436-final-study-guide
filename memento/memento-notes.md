# Memento Design Pattern
A pattern used to **save** the state of an object and allow for
**rollbacks**.

![Memento Diagram](memento-diagram.png)
