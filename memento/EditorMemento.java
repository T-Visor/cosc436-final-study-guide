import java.util.List;
import java.util.ArrayList;

/**
 * Represents a saved snapshot of an Editor object's state.
 */
public class EditorMemento 
{
    private final String editorState;

    /**
     * Create a memento instance.
     *
     * @param state the information to store
     */
    public EditorMemento(String state)
    {
        editorState = state;
    }
    
    /**
     * @return the saved information of an Editor object.
     */
    public String getSavedState()
    {
        return editorState;
    }
}
