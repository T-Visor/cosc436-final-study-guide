/**
 * Represents a text editor which can temporarily store one piece of
 * information at a time.
 */
public class Editor 
{
    public String editorContents;

    /**
     * Store the information passed to this method call.
     *
     * @param contents the string contents to store
     */
    public void setState(String contents)
    {
        this.editorContents = contents;
    }

    /**
     * @return a memento of this Editor object's saved state.
     */
    public EditorMemento save()
    {
        return new EditorMemento(editorContents);
    }

    /**
     * Restore the information from a prior state using the information passed
     * to this method call.
     *
     * @param memento the prior state of an Editor object
     */
    public void restoreToState(EditorMemento memento)
    {
        editorContents = memento.getSavedState();
    }

    /**
     * @return a string representation of this Editor object's state.
     */
    public String toString()
    {
        return editorContents;
    }
}
