import java.util.List;
import java.util.ArrayList;

public class Driver 
{
    public static void main(String[] args)
    {
        // Create an editor and an empty collection to hold saved states 
        List<EditorMemento> savedStates = new ArrayList<EditorMemento>();
        Editor editor = new Editor();

        // Add 'Chicken' state
        System.out.println("Saving 'Chicken' to memento...");
        editor.setState("State: Chicken");
        savedStates.add(editor.save());

        // Add 'Akane' state
        System.out.println("Saving 'Akane' to memento...");
        editor.setState("State: Akane");
        savedStates.add(editor.save());

        // Add 'Giggle' state
        System.out.println("Saving 'Giggle' to memento...\n");
        editor.setState("State: Giggle");
        savedStates.add(editor.save());

        // Display all the saved states.
        for (EditorMemento state : savedStates)
            System.out.println(state.getSavedState());

        // Restore the state at index 1 and print the editor's contents
        editor.restoreToState(savedStates.get(1));
        System.out.println("\nRestoring state at index 1 of savedStates - " + editor);
    }
}
