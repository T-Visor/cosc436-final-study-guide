/**
 * Represents an item at an online shopping store.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class Item 
{
	private String upcCode;
	private int price;
	
    /**
     * Create an item instance.
     *
     * @param upc the universal product code
     * @param cost the cost of the item in whole dollars
     */
	public Item(String upc, int cost)
    {
		this.upcCode=upc;
		this.price=cost;
	}

    /**
     * @return the universal product code.
     */
	public String getUpcCode() 
    {
		return upcCode;
	}

    /**
     * @return the price in whole dollars.
     */
	public int getPrice() 
    {
		return price;
	}
}
