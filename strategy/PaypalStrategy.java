/**
 * Represents a PayPal payment processor.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class PaypalStrategy implements PaymentStrategy
{
    private String email;
    private String password;
    
    /**
     * Create a PayPal payment processor.
     *
     * @param email the email of the client
     * @param password the password of the client
     */
    public PaypalStrategy(String email, String password)
    {
        this.email = email;
        this.password = password;
    }

    /**
     * Submit a payment using PayPal.
     *
     * @param amount the amount in whole dollars
     */
    public void pay (int amount)
    {
        System.out.println(amount + " paid using paypal");
    }
}
