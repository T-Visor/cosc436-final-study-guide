/**
 * This interface represents a common operation which
 * all implementor classes must redefine.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public interface PaymentStrategy 
{
    /**
     * The payment operation which will 
     * be redefined in implementor classes.
     *
     * @param amount the cost in whole dollars
     */
    public void pay (int amount);
}
