import java.util.*;
import java.text.DecimalFormat;

/**
 * Represents a shopping cart for a client
 * shopping at an online store.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class ShoppingCart 
{
	List<Item> items;
	
    /**
     * Create a new shopping cart instance.
     */
	public ShoppingCart()
    {
		this.items = new ArrayList<Item>();
	}
	
    /**
     * Add an item to the shopping cart.
     *
     * @param item the item to add
     */
	public void addItem(Item item)
    {
		this.items.add(item);
	}
	
    /**
     * Remove an item from the shopping cart.
     *
     * @param item the item to remove
     */
	public void removeItem(Item item)
    {
		this.items.remove(item);
	}
	
    /**
     * @return the total price of all items in the shopping cart.
     */
	public int calculateTotal()
    {
		int sum = 0;

		for(Item item : items)
			sum += item.getPrice();

		return sum;
	}
	
    /**
     * Submit a payment using a payment processor.
     *
     * @param paymentMethod the payment processor to use
     */
	public void pay(PaymentStrategy paymentMethod)
    {
		int amount = calculateTotal();
		paymentMethod.pay(amount);
	}
}
