/**
 * Represents a credit card payment processor. 
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class CreditCardStrategy implements PaymentStrategy
{
    private String cardHolderName;
    private String cardNumber;
    private String expirationDate;
    
    /**
     * Create a credit card payment processor.
     *
     * @param cardHolderName the name of the card holder 
     * @param cardNumber the 16 digit credit card number
     * @param expirationDate the expiration date of the credit card
     */
    public CreditCardStrategy(String cardHolderName, String cardNumber, String expirationDate)
    {
        this.cardHolderName = cardHolderName;
        this.cardNumber = cardNumber;
        this.expirationDate = expirationDate;
    }

    /**
     * Submit a payment using the credit card.
     *
     * @param amount in whole dollars
     */
    public void pay(int amount)
    {
        System.out.println(amount + " paid with credit card");
    }
}
