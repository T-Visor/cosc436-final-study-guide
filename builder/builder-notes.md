# Builder Design Pattern
Moves the construction process out of the object class to a seperate **Builder**
class.

There can be more than one Builder class for building a given object type in
various ways.

The building process is broken down into a *number of steps*. Each step can be
defined by a separate method of a common **Builder interface**.

![Builder Diagram](builder-diagram.png)
