public class HawaiianPizzaBuilder extends PizzaBuilder
{
    public HawaiianPizzaBuilder()
    {
    }

    public void buildDough()
    {
        pizza.setDough("Hawaiian pizza dough!"); 
    }

    public void buildSauce()
    {
        pizza.setSauce("Hawaiian pizza sauce!");
    }

    public void buildTopping()
    {
        pizza.setTopping("Hawaiian toppings like Pineapples!");
    }
}
