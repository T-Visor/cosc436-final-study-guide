public class SpicyPizzaBuilder extends PizzaBuilder
{
    public SpicyPizzaBuilder()
    {
    }

    public void buildDough()
    {
        pizza.setDough("Dough for spicy pizza!"); 
    }

    public void buildSauce()
    {
        pizza.setSauce("Spicy sauce!");
    }

    public void buildTopping()
    {
        pizza.setTopping("Spicy toppings!");
    }
}
