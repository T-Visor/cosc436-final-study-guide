public class Client
{
    public static void main(String[] args)
    {
        Waiter waiter = new Waiter();

        // Construct a Hawaiian pizza.
        System.out.println("** Hawaiian Pizza **");
        waiter.setPizzaBuilder(new HawaiianPizzaBuilder());   
        waiter.constructPizza();
        System.out.println(waiter.getPizza());

        // Construct a Spicy pizza.
        System.out.println("** Spicy Pizza **");
        waiter.setPizzaBuilder(new SpicyPizzaBuilder());   
        waiter.constructPizza();
        System.out.println(waiter.getPizza());
    }
}
