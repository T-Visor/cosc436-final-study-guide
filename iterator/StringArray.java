import java.lang.UnsupportedOperationException;
import java.util.Iterator;
import java.util.ArrayList;

/**
 * This class and its inner classes demonstrate an implementation
 * of the Iterator pattern.
 *
 * StringArray is simply a wrapper class for an array of Strings.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class StringArray implements Iterable<String>
{
    private ArrayList<String> values;

    /**
     * Creates a single instance of an array of Strings
     *
     * @param values the list of values to assign to this StringArray
     */
    public StringArray(ArrayList<String> values)
    {
        this.values = values;
    }

    /**
     * @return an Iterator which can traverse StringArrays
     */
    public Iterator<String> iterator()
    {
        return new LongStringIterator();
    }

//==================================================================================

    /**
     * (PRIVATE INNER CLASS)
     *
     * This class creates objects which can iterate through StringArrays. 
     */
    private class ArrayIterator implements Iterator<String>
    {
        private int current;

        /**
         * Create a single iterator instance which will point to
         * the first element of a StringArray.
         */
        public ArrayIterator()
        {
            current = 0;
        }

        /**
         * @return 'true' if there is another item in the ArrayList to process
         *         'false' otherwise.
         */
        public boolean hasNext()
        {
            return current < values.size();
        }

        /**
         * @return the next element in the ArrayList.
         */
        public String next()
        {
            String temp = values.get(current);
            current++;
            return temp;
        }

        /**
         * This operation is not supported, normally it would be used to
         * remove an element from the ArrayList.
         */
        public void remove() throws UnsupportedOperationException
        {
            throw new UnsupportedOperationException();
        }
    }
   
//==================================================================================

    /**
     * (PRIVATE INNER CLASS)
     *
     * This class creates objects which can iterate through StringArrays,
     * but will only retrieve String objects with length >= 4.
     */
    private class LongStringIterator implements Iterator<String>
    {
        private int current;

        /**
         * Create a single iterator instance which will point to
         * the first element of a StringArray.
         */
        public LongStringIterator()
        {
            current = 0;
        }

        /**
         * @return 'true' if there is another item in the ArrayList to process
         *         'false' otherwise.
         */
        public boolean hasNext()
        {
            if (current < values.size())
            {
                while (values.get(current).length() < 4)
                {
                    current++;

                    if (current >= values.size())
                        return false;
                }
                return true;
            }
            else
                return false;
        }

        /**
         * @return the next String element in the ArrayList
         *         if its length is >= 4 or an empty String.    
         */
        public String next()
        {
            String temp = values.get(current);
            current++;
            return temp;
        }

        /**
         * This operation is not yet supported, normally it would be used to
         * remove an element from the ArrayList.
         */
        public void remove() throws UnsupportedOperationException
        {
            throw new UnsupportedOperationException();
        }
    }
}
