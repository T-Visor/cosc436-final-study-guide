import java.util.Iterator;
import java.util.ArrayList;

/**
 * Test the StringArray class.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class TestStringArray
{
    public static void main(String[] args)
    {
        // create an ArrayList and populate it with elements
        ArrayList<String> elements = new ArrayList<String>();
        elements.add("hello");
        elements.add("there");
        elements.add("world");
        elements.add("great");

        // create a 'StringArray' instance
        StringArray listOfStrings = new StringArray(elements);

        // create an iterator for 'StringArray'
        Iterator<String> iterator = listOfStrings.iterator();

        // test the iterator by printing out all contents
        while (iterator.hasNext())
            System.out.println(iterator.next());
    }
}
