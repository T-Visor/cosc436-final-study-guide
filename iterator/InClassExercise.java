import java.util.Iterator;
import java.util.ArrayList;
import java.util.TreeSet;

public class InClassExercise
{
    public static void main(String[] args)
    {
        // Create the array list
        TreeSet<String> stringArrayList = new TreeSet<String>();
        
        // add elements to the array list
        stringArrayList.add("one");
        stringArrayList.add("two");
        stringArrayList.add("three");
        stringArrayList.add("four");
        stringArrayList.add("five");

        // print using for loop (only applicable to ArrayList)
        /*
        System.out.println("Print using for loop: ");
        for (int i = 0; i < stringArrayList.size(); i++)
            System.out.println(stringArrayList.get(i));
        */

        System.out.println();

        // Create the iterator 
        Iterator<String> arrayListIterator = stringArrayList.iterator();
        
        // print using iterator
        System.out.println("Print using arrayListIterator: ");
        while (arrayListIterator.hasNext()) 
            System.out.println(arrayListIterator.next());
    }
}
