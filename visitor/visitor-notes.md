# Visitor Design Pattern
A pattern used to represent (and separate) an **algorithm** from an object
structure.

This allows the programmer to define a new operation on for an object without changing its 
underlying class implementation.

![Visitor Diagram](visitor-diagram.png)
