# Visitor Design Pattern
The facade pattern hides the complexities of a given system and provides
a **simplified interface** which the client can use to access the system.

The pattern involves the usage of **one class**, which provides simplified methods
required by the client, and delegates calls to methods for an existing system of
classes.
![Facade Diagram](facade-diagram.png)
