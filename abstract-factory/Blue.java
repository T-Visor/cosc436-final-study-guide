/**
 * Concrete color product for 'blue'.
 */
public class Blue implements Color
{
    public void fill()
    {
        System.out.println("inside Blue.fill() method"); 
    }
}
