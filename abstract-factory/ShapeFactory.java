/**
 * Concrete implementation of a factory which produces shape products.
 */
public class ShapeFactory extends AbstractFactory
{
    /**
     * @return a new Shape according to the parameter.
     *
     * @param shapeName the name of the desired shape 
     *                  (CIRCLE, SQUARE, RECTANGLE)
     */
    public Shape getShape(String shapeName)
    {
        switch (shapeName)
        {
            case "CIRCLE":
                return new Circle();
            case "SQUARE":
                return new Square();
            case "RECTANGLE":
                return new Rectangle();
            default:
                return new Circle();
        }
    }

    /**
     * This method is not supported.
     */
    public Color getColor(String color)
    {
        throw new UnsupportedOperationException();
    }
}
