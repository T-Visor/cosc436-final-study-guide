/**
 * Test the components of the abstract factory design pattern.
 */
public class AbstractFactoryPatternDemo 
{
    public static void main(String[] args) 
    {
        Shape shape;
        Color color;
        AbstractFactory factory;

        //----------------------SHAPE FACTORY----------------------------      
        factory = FactoryProducer.getFactory("SHAPE");

        shape = factory.getShape("CIRCLE");
        shape.draw();

        shape = factory.getShape("RECTANGLE");
        shape.draw();

        shape = factory.getShape("SQUARE");
        shape.draw();

        System.out.println();

        //----------------------COLOR FACTORY----------------------------
        factory = FactoryProducer.getFactory("COLOR");

        color = factory.getColor("RED");
        color.fill();

        color = factory.getColor("GREEN");
        color.fill();

        color = factory.getColor("BLUE");
        color.fill();
    }
}

