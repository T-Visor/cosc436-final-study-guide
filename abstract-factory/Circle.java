/**
 * Concrete shape product for 'circle'.
 */
public class Circle implements Shape
{
    public void draw()
    {
        System.out.println("inside Circle.draw() method");
    }
}
