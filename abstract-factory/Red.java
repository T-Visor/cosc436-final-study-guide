/**
 * A concrete color product for 'red'.
 */
public class Red implements Color
{
    public void fill()
    {
        System.out.println("inside Red.fill() method"); 
    }
}
