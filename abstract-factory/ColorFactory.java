/**
 * Concrete implementation of a factory which produces color products.
 */
public class ColorFactory extends AbstractFactory
{
    /**
     * @return a new Color according to the parameter.
     *
     * @param shapeName the name of the desired Color
     *                  (RED, GREEN, BLUE)
     */
    public Color getColor(String colorName)
    {
        switch (colorName)
        {
            case "RED":
                return new Red();
            case "GREEN":
                return new Green();
            case "BLUE":
                return new Blue();
            default:
                return new Red();
        }
    }

    /**
     * This operation is not supported.
     */
    public Shape getShape(String shape)
    {
        throw new UnsupportedOperationException();
    }
}
