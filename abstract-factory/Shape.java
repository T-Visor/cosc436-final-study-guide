/**
 * Defines an interface for 'shape' products.
 */
public interface Shape
{
    public void draw();
}
