/**
 * Defines an interface for 'color' products.
 */
public interface Color
{
    public void fill();
}
