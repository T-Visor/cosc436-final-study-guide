/**
 * Creates a concrete factory based on the need of the client.
 */
public class FactoryProducer
{
    /**
     * @return a ShapeFactory if choice is "SHAPE" 
     *         or a ColorFactory if choice is "COLOR"
     * 
     * @param choice the description "SHAPE" or "COLOR"
     */
    public static AbstractFactory getFactory(String choice)
    {
        if(choice.equalsIgnoreCase("SHAPE"))
            return new ShapeFactory();  
        else if(choice.equalsIgnoreCase("COLOR"))
            return new ColorFactory();
        return null;
    }
}
