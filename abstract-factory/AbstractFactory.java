/**
 * Define the abstract factory, subclasses
 * will determine which products will be produced.
 */
public abstract class AbstractFactory
{
    /**
     * @return a Color object based on the description.
     *
     * @param color the name of the color
     */
    public abstract Color getColor(String color);

    /**
     * @return a Shape object based on the description passed.
     *
     * @param shape the name of the shape
     */
    public abstract Shape getShape(String shape);
}
