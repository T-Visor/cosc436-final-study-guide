/**
 * A concrete shape product for 'square'.
 */
public class Square implements Shape
{
    public void draw()
    {
        System.out.println("inside Square.draw() method");
    }
}
