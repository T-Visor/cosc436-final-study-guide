/**
 * Represents a concrete command to switch a light to its 'on' position.
 */
public class LightOnCommand implements Command
{
    Light light;

    /**
     * Create the concrete command object.
     */
    public LightOnCommand(Light light)
    {
        this.light = light;
    }

    /**
     * Switch the light on.
     */
    public void execute()
    {
        light.switchOn();
    }
}
