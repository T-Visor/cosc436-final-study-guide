/**
 * Represents a concrete command to switch a light to its 'off' position.
 */
public class LightOffCommand implements Command
{
    Light light;

    /**
     * Create the concrete command object.
     */
    public LightOffCommand(Light light)
    {
        this.light = light;
    }

    /**
     * Switch the light off.
     */
    public void execute()
    {
        light.switchOff();
    }
}
