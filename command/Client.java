/**
 * Demonstrate the Command design pattern.
 */
public class Client
{
    /**
     * Run a demonstration of the Command design pattern.
     */
    public static void main(String[] args)    
    {
        RemoteControl control = new RemoteControl();
        Light light = new Light();

        // Concrete commands for switching the light off and on.
        Command turnLightOn = new LightOnCommand(light);
        Command turnLightOff = new LightOffCommand(light);

        // Switch the light on using the remote control.
        System.out.println("*** SWITCHING LIGHT ON ***");
        control.setCommand(turnLightOn);
        control.pressButton();
        displayLightStatus(light);

        System.out.println();

        // Switch the light off using the remote control.
        System.out.println("*** SWITCHING LIGHT OFF ***");
        control.setCommand(turnLightOff);
        control.pressButton();
        displayLightStatus(light);
    }

    /**
     * Light can be either 'On' or 'Off'
     *
     * @param light the light object
     */
    public static void displayLightStatus(Light light)
    {
        System.out.print("Light status: ");
        System.out.println((light.isOn()) ? "On" : "Off");
    }
}

