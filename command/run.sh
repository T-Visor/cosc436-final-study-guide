#!/bin/sh

# compile and run the java program
javac *.java && java Client

# cleanup .class files after running
rm *.class
