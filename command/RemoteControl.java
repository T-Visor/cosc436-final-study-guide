/**
 * Represents a controller with programmable commands.
 */
public class RemoteControl
{
    private Command command;

    /**
     * Set the desired concrete command.
     *
     * @param command the concrete command object
     */
    public void setCommand(Command command)
    {
        this.command = command;
    }

    /**
     * Run the command.
     */
    public void pressButton()
    {
        command.execute();
    }
}
