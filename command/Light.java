/**
 * Represents an instance of illumination equipment which can 
 * switch on or off.
 */
public class Light
{
    private boolean on;

    /**
     * Switch the light to on.
     */
    public void switchOn()
    {
        on = true;
    }

    /**
     * Switch the light off.
     */
    public void switchOff()
    {
        on = false;
    }

    /**
     * @return 'true' if the light is on, 'false' otherwise.
     */
    public boolean isOn()
    {
        return on;
    }
}
