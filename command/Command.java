/**
 * Objects implementing this interface represent a concrete command
 * which acts upon another object.
 */
public interface Command
{
    /**
     * Run the command for the object who has an associative
     * relationship to this one.
     */
    public void execute();
}
