
/**
 * A bad example of using classes without an adapter,
 * violates the open-close principle.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
public class AdapterDemo_BadExample 
{
	public static void main(String[] args) 
    {
		Object[] shapes = { new Line(), new Rectangle() };

		int x1 = 10, y1 = 20;
		int x2 = 30, y2 = 60;
		int width = 40, height = 40;

		for (Object shape : shapes) 
        { 
            // Need to check type... NOT GOOD 
			if (shape.getClass().getSimpleName().equals("Line")) 
				((Line) shape).draw(x1, y1, x2, y2);
			else if (shape.getClass().getSimpleName().equals("Rectangle"))
				((Rectangle) shape).draw(x2, y2, width, height);
		}
	}
}
