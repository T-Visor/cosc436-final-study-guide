
/**
 * The interface which must be adapted to in the system.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
interface Shape 
{
	void draw(int x, int y, int z, int j);
}
