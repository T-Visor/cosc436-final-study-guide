/**
 * Represents a rectangle on a graph.
 *
 * @author Alexander Dao
 * @author Turhan Kimbrough
 */
class Rectangle 
{
	public void draw(int x, int y, int width, int height) 
    {
		System.out.println("Rectangle with coordinate left-down point (" + x + ";" + y + "), width: " + width
				+ ", height: " + height);
	}
}
