public class MediatorDemo 
{
    public static void main(String[] args) 
    {
        User firstUser = new User("Makoto");
        User secondUser = new User("Akira");

        firstUser.sendMessage("Hi! Akira!");
        secondUser.sendMessage("Hello! Makoto!");
    }
}
